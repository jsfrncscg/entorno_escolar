#!/bin/bash

mkdir -p /var/www/html/escolar/web/assets
mkdir -p /var/www/html/escolar/web/protected/runtime 
mkdir -p /var/www/html/escolar/web/public/uploads/fundamentoJuridico 
mkdir -p /var/www/html/escolar/web/public/uploads/ticket 
mkdir -p /var/www/html/escolar/web/public/uploads/instructivo 
mkdir -p /var/www/html/escolar/web/public/upload/titulo 
mkdir -p /var/www/html/escolar/web/public/downloads 
mkdir -p /var/www/html/escolar/web/public/downloads/qr/tituloDigital 
mkdir -p /var/www/html/escolar/web/public/downloads/pdf/tituloDigital

FROM debian:jessie

RUN apt-get update && apt-get install -y php5 apache2 libapache2-mod-php5 php5-pgsql php5-sqlite php5-xsl php5-gd nano vim openssh-server sudo

# Copiando virtualhost
ADD ./escolar.conf /etc/apache2/sites-available/

# Activando debug de errores
RUN sed -i -e's/display_errors = Off/display_errors = On/g' /etc/php5/apache2/php.ini

# Activando sitio escolar.conf
#RUN a2ensite /etc/apache2/sites-available/escolar.conf
RUN ln -s /etc/apache2/sites-available/escolar.conf /etc/apache2/sites-enabled/escolar.conf

RUN a2enmod rewrite

RUN useradd -m entorno && echo "entorno:entorno" | chpasswd && adduser entorno sudo

ADD ./start-env /bin/start-env
RUN chmod +x /bin/start-env

ENTRYPOINT ["/bin/start-env"]